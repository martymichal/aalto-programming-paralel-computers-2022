# Programming Parallel Computers 2022

- 5th period; spring semester
- Passed with 1/5
- One of the most enjoyable coruses at Aalto.

## Quick Summary

The course taught us several techniques for writing performant programs on the CPU:

- instruction-level paralelism
- multithreading
- vector operations

After learning how to use these the next step was to learn how to write IO-efficient programs. This part is probably the most difficult one as it requires good visualization skills and good understanding of algorithms that are being optimized.

Another major chapter was programming on the GPU where the style of programming is slightly different. The course focused aminly on CUDA but also provided materials for OpenCL. IO was a vital part of the chapter as well.

## Future

- working on some of the unfinished exercises
- optimizing unfinished exercises
- studying OpenCL
