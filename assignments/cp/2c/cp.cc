#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <vector>

#include "vector.h"

constexpr int cell_size = 4;

static void normalize_matrix(int ny, int nx, const float *data,
                             std::vector<double4_t> *output) {
  int cells_n = (nx + cell_size - 1) / cell_size;

  for (int y = 0; y < ny; y++) {
    double sum = 0, mean = 0;
    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::lowest();

    for (int x = 0; x < nx; x++) {
      double xx = data[nx * y + x];

      min = std::min(min, xx);
      max = std::max(max, xx);
      mean += xx;
    }
    mean /= nx;

    // Arithmetic mean == 0
    for (int x = 0; x < nx; x++) {
      double xx = data[nx * y + x];

      double val = (xx - mean) / (max - min);
      (*output)[cells_n * y + x / cell_size][x % cell_size] = val;
      sum += std::pow(val, 2);
    }
    sum = std::sqrt(sum);

    // Sum of squares == 1
    for (int x = 0; x < cells_n; x++) {
      (*output)[cells_n * y + x] /= sum;
    }
  }
}

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in data[x + y*nx]
- correlation between rows i and row j has to be stored in result[i + j*ny]
- only parts with 0 <= j <= i < ny need to be filled
*/
void correlate(int ny, int nx, const float *data, float *result) {
  int cells_n = (nx + cell_size - 1) / cell_size;
  int normdata_size = cells_n * ny;

  std::vector<double4_t> normdata(normdata_size, double4_0);

  normalize_matrix(ny, nx, data, &normdata);

  // product of normdata * normdata(transposed)
  for (int ay = 0; ay < ny; ay++) {   // A row
    for (int by = 0; by < ny; by++) { // B row
      if (by < ay) {
        continue;
      }

      double4_t res_parts = double4_0;
      for (int i = 0; i < cells_n; i++) { // B column
        res_parts += normdata[cells_n * ay + i] * normdata[cells_n * by + i];
      }

      double res = 0;
      for (int i = 0; i < cell_size; i++) {
        res += res_parts[i];
      }
      result[ay * ny + by] = res;
    }
  }
}
