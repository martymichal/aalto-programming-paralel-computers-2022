#include <algorithm>
#include <cmath>
#include <limits>
#include <vector>

static void normalize_matrix(int ny, int nx, const float *data,
                             std::vector<double> *output) {
  for (int i = 0; i < ny; i++) {
    double sum = 0, mean = 0;
    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::lowest();

    for (int x = 0; x < nx; x++) {
      double xx = data[nx * i + x];

      min = std::min(min, xx);
      max = std::max(max, xx);
      mean += xx;
    }
    mean /= nx;

    // Arithmetic mean == 0
    for (int x = 0; x < nx; x++) {
      double xx = data[nx * i + x];

      (*output)[nx * i + x] = (xx - mean) / (max - min);
      sum += std::pow((*output)[nx * i + x], 2);
    }
    sum = std::sqrt(sum);

    // Sum of squares == 1
    for (int x = 0; x < nx; x++) {
      (*output)[nx * i + x] /= sum;
    }
  }
}

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in data[x + y*nx]
- correlation between rows i and row j has to be stored in result[i + j*ny]
- only parts with 0 <= j <= i < ny need to be filled
*/
void correlate(int ny, int nx, const float *data, float *result) {
  std::vector<double> normdata(ny * nx);

  normalize_matrix(ny, nx, data, &normdata);

  // product of normdata * normdata(transposed)
  for (int ay = 0; ay < ny; ay++) {   // A row
    for (int by = 0; by < ny; by++) { // B row
      if (by < ay) {
        continue;
      }

      double res = 0;
      for (int bx = 0; bx < nx; bx++) { // B column
        res += normdata[ay * nx + bx] * normdata[by * nx + bx];
      }
      result[ay * ny + by] = res;
    }
  }
}
