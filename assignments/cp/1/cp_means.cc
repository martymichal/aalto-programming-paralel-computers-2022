#include <cmath>
#include <iostream>
#include <vector>

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in data[x + y*nx]
- correlation between rows i and row j has to be stored in result[i + j*ny]
- only parts with 0 <= j <= i < ny need to be filled
*/
void correlate(int ny, int nx, const float *data, float *result) {
  std::vector<double> row_means(ny, -1);

  for (int i = 0; i < ny; i++) {
    if (row_means[i] == -1) {
      double val = 0;
      for (int x = 0; x < nx; x++) {
        val += data[nx * i + x];
      }
      row_means[i] = val / nx;
    }

    double meanx = row_means[i];

    double sumx2 = 0;
    for (int x = 0; x < nx; x++) {
      double xx = data[nx * i + x];
      sumx2 += std::pow(xx, 2);
    }
    double meanx_pow = std::pow(meanx, 2);

    for (int j = 0; j < ny; j++) {
      double sumxy = 0, sumx2 = 0, sumy2 = 0;

      if (i < j) {
        continue;
      }

      if (row_means[j] == -1) {
        double val = 0;
        for (int x = 0; x < nx; x++) {
          val += data[nx * j + x];
        }
        row_means[j] = val / nx;
      }

      double meany = row_means[j];

      for (int x = 0; x < nx; x++) {
        double xx = data[nx * i + x];
        double yx = data[nx * j + x];

        sumxy += xx * yx;
        sumy2 += std::pow(yx, 2);
      }

      double nom = sumxy - nx * meanx * meany;
      double denom = std::sqrt(sumx2 - nx * meanx_pow) *
                     std::sqrt(sumy2 - nx * std::pow(meany, 2));

      double res = nom / denom;

      result[i + j * ny] = res;
    }
  }
}
