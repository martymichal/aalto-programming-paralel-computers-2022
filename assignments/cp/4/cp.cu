#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <vector>

#include <cuda_runtime.h>

#define CHECK(x) check(x, #x)

constexpr float infty = std::numeric_limits<float>::infinity();
constexpr float lowest = std::numeric_limits<float>::lowest();

constexpr int block_size = 16;

static inline int divup(int a, int b) {
    return (a + b - 1) / b;
}

static inline void check(cudaError_t err, const char *context) {
    if (err != cudaSuccess) {
        std::cerr << "CUDA error: " << context << ": "
            << cudaGetErrorString(err) << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

static void normalize_matrix(int ny, int nx, const float *data, std::vector<float> *output) {
  double sums[ny], means[ny], mins[ny], maxs[ny];

  for (int i = 0; i < ny; i++) {
    double row[nx];

    sums[i] = 0;
    means[i] = 0;
    mins[i] = infty;
    maxs[i] = lowest;

    for (int x = 0; x < nx; x++) {
      double xx = data[nx * i + x];

      mins[i] = std::min(mins[i], xx);
      maxs[i] = std::max(maxs[i], xx);
      means[i] += xx;
    }
    means[i] /= nx;

    // Arithmetic mean == 0
    for (int x = 0; x < nx; x++) {
      double xx = data[nx * i + x];

      double val = (xx - (means[i])) / (maxs[i] - mins[i]);

      row[x] = val;
      sums[i] += std::pow(val, 2);
    }
    sums[i] = std::sqrt(sums[i]);

    // Sum of squares == 1
    for (int x = 0; x < nx; x++) {
      (*output)[nx * i + x] = row[x] / sums[i];
    }
  }
}

__global__ void gpu_correlate_kernel(int ny, int nx, const float *data, float *result) {
  int ay = blockIdx.x * blockDim.x + threadIdx.x;
  int by = blockIdx.y * blockDim.y + threadIdx.y;

  if (ay >= ny || by >= ny || by < ay) {
    return;
  }

  float res = 0;
  for (int bx = 0; bx < nx; bx++) {
    float A = data[ay * nx + bx];
    float B = data[by * nx + bx];

    res += A * B;
  }

  result[ay * ny + by] = res;
}

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in data[x + y*nx]
- correlation between rows i and row j has to be stored in result[i + j*ny]
- only parts with 0 <= j <= i < ny need to be filled
*/
void correlate(int ny, int nx, const float *data, float *result) {
  std::vector<float> normdata(ny * nx);
  normalize_matrix(ny, nx, data, &normdata);

  float *data_gpu = nullptr;
  CHECK(cudaMalloc((void**) &data_gpu, ny * nx * sizeof(float)));
  CHECK(cudaMemcpy(data_gpu, normdata.data(), ny * nx * sizeof(float), cudaMemcpyHostToDevice));

  float *result_gpu = nullptr;
  CHECK(cudaMalloc((void**) &result_gpu, ny * ny * sizeof(float)));
  CHECK(cudaMemset(result_gpu, 0, ny * ny * sizeof(float)));

  dim3 block(block_size, block_size);
  dim3 block_grid(divup(ny, block.x), divup(ny, block.y));
  gpu_correlate_kernel<<<block_grid, block>>>(ny, nx, data_gpu, result_gpu);
  CHECK(cudaGetLastError());

  CHECK(cudaMemcpy(result, result_gpu, ny * ny * sizeof(float), cudaMemcpyDeviceToHost));

  CHECK(cudaFree(data_gpu));
  CHECK(cudaFree(result_gpu));
}
