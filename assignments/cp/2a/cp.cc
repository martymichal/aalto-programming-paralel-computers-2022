#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <numeric>
#include <vector>

constexpr int np = 32;
constexpr double infty = std::numeric_limits<double>::infinity();
constexpr double lowest = std::numeric_limits<double>::lowest();

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in data[x + y*nx]
- correlation between rows i and row j has to be stored in result[i + j*ny]
- only parts with 0 <= j <= i < ny need to be filled
*/
void correlate(int ny, int nx, const float *data, float *result) {
  int nxn = (nx + np - 1) / np;
  int npnx = nxn * np;

  double sums[ny], means[ny], mins[ny], maxs[ny];
  std::vector<double> normdata(ny * npnx, 0);

  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < ny; i++) {
    sums[i] = 0;
    means[i] = 0;
    mins[i] = infty;
    maxs[i] = lowest;

    for (int x = 0; x < nx; x++) {
      double xx = data[nx * i + x];

      mins[i] = std::min(mins[i], xx);
      maxs[i] = std::max(maxs[i], xx);
      means[i] += xx;
    }
    means[i] /= nx;

    // Arithmetic means[i] == 0
    for (int x = 0; x < nx; x++) {
      double xx = data[nx * i + x];

      normdata[npnx * i + x] = (xx - means[i]) / (maxs[i] - mins[i]);
      sums[i] += std::pow(normdata[npnx * i + x], 2);
    }
    sums[i] = std::sqrt(sums[i]);

    // sums[i] of squares == 1
    for (int x = 0; x < nx; x++) {
      normdata[npnx * i + x] /= sums[i];
    }
  }

  auto end = std::chrono::high_resolution_clock::now();
  std::cerr << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start)
                   .count()
            << std::endl;

  for (int ay = 0; ay < ny; ay++) {
    for (int by = 0; by < ny; by++) {
      if (by < ay) {
        continue;
      }

      double ress[np];
      for (int ip = 0; ip < np; ip++) {
        ress[ip] = 0;
      }

      for (int ipx = 0; ipx < nxn; ipx++) {
        for (int ip = 0; ip < np; ip++) {
          ress[ip] += normdata[npnx * ay + ipx * np + ip] *
                      normdata[npnx * by + ipx * np + ip];
        }
      }

      double res = 0;
      for (int ip = 0; ip < np; ip++) {
        res += ress[ip];
      }

      result[ay * ny + by] = res;
    }
  }

  end = std::chrono::high_resolution_clock::now();
  std::cerr << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start)
                   .count()
            << std::endl;
}
