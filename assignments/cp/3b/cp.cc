#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <vector>

#include "vector.h"

constexpr int vec_size = 8;
constexpr int block_size = 6;
constexpr float infty = std::numeric_limits<float>::infinity();
constexpr float lowest = std::numeric_limits<float>::lowest();

static void normalize_matrix(int ny, int nx, const float *data,
                             int nnx, std::vector<float8_t> *output) {
  double sums[ny], means[ny], mins[ny], maxs[ny];

#pragma omp parallel for schedule(static, 1)
for (int y = 0; y < ny; y++) {
  int col_vec_n = (nx + vec_size - 1) / vec_size; // num of vectors in a row
    double row[nx];
    sums[y] = 0;
    means[y] = 0;
    mins[y] = infty;
    maxs[y] = lowest;

    for (int x = 0; x < nx; x++) {
      double xx = data[nx * y + x];

      mins[y] = std::min(mins[y], xx);
      maxs[y] = std::min(maxs[y], xx);
      means[y] += xx;
    }
    means[y] /= nx;

    // Arithmetic mean == 0
    for (int x = 0; x < nx; x++) {
      double xx = data[nx * y + x];

      double val = (xx - means[y]) / (maxs[y] - mins[y]);

      // std::cout << val << " ";
      //(*output)[nnx * y + x / vec_size][x % vec_size] = val;
      row[x] = val;
      sums[y] += std::pow(val, 2);
    }
    sums[y] = std::sqrt(sums[y]);

    // Sum of squares == 1
    for (int x = 0; x < nx; x++) {
      (*output)[nnx * y + x / vec_size][x % vec_size] = row[x] / sums[y];
      // (*output)[nnx * y + x] /= float(sums[y]);
    }
  }
}

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in data[x + y*nx]
- correlation between rows i and row j has to be stored in result[i + j*ny]
- only parts with 0 <= j <= i < ny need to be filled
*/
void correlate(int ny, int nx, const float *data, float *result) {
  int col_vec_n = (nx + vec_size - 1) / vec_size; // num of vectors in a row

  int row_block_n = (ny + block_size - 1) / block_size;
  int col_block_n = (col_vec_n + block_size - 1) / block_size;

  int row_vec_n = row_block_n * block_size;
  col_vec_n = col_block_n * block_size;

  int normdata_size = row_vec_n * col_vec_n; // num of vectors in the whole matrix

  /*std::cout << "R: " << row_block_n << " C: " << col_block_n
            << " S: " << normdata_size << std::endl;*/
  std::vector<float8_t> normdata(normdata_size, float8_0);

  auto start = std::chrono::steady_clock::now();
  normalize_matrix(ny, nx, data, col_vec_n, &normdata);
  auto end = std::chrono::steady_clock::now();
  std::cerr << (end - start).count() << std::endl;

  asm("# start working");
  #pragma omp parallel for collapse(2) schedule(static, 1)
  for (int ay = 0; ay < row_block_n; ay++) {
    for (int by = 0; by < row_block_n; by++) {
      if (by < ay) {
        continue;
      }

      int A_block_i_base = ay * col_vec_n * block_size;
      int B_block_i_base = by * col_vec_n * block_size;
      // std::cout << std::endl << "AY: " << ay << " - " << A_block_i_base << " BY: " << by << " - " << B_block_i_base << std::endl;

      float8_t ress[block_size][block_size];
      asm("# prepare accumulators");
      for (int br = 0; br < block_size; br++) {
        for (int brr = 0; brr < block_size; brr++) {
          ress[br][brr] = float8_0;
        }
      }

      // TODO(V2): Better use caches
      asm("# columns loop");
      for (int x = 0; x < col_block_n; x++) {
        float8_t A[block_size][block_size];
        float8_t B[block_size][block_size];

        // std::cout << std::endl << "bc: " << x << std::endl << std::endl;
        asm("# read data in blocks");
        for (int br = 0; br < block_size; br++) {
          for (int bc = 0; bc < block_size; bc++) {
            int pos_A = A_block_i_base + br * col_vec_n + x * block_size + bc;
            int pos_B = B_block_i_base + br * col_vec_n + x * block_size + bc;
            /*std::cout << "BR: " << br << " - A: " << pos_A << " - B: " << pos_B
                      << std::endl;*/
            A[br][bc] = normdata[pos_A];
            B[br][bc] = normdata[pos_B];
          }
        }

        asm("# count");
        for (int ar = 0; ar < block_size; ar++) {
          int A_pos_ref = A_block_i_base + ar * col_vec_n;
          for (int br = 0; br < block_size; br++) {
            int B_pos_ref = B_block_i_base + br * col_vec_n;
            if (B_pos_ref < A_pos_ref) {
              continue;
            }

            for (int bc = 0; bc < block_size; bc++) {
              ress[ar][br] += A[ar][bc] * B[br][bc];
              /*for (int i = 0; i < vec_size; i++) {
                std::cout << A[ar][bc][i] << " " << B[br][bc][i];
              }
              std::cout << std::endl;*/
            }
          }
        }
      }

      asm("# save results");
      for (int br = 0; br < block_size; br++) {
        int res_x = ay * ny * block_size + br * ny;

        // std::cout << "rx: " << res_x; 
        // std::cout.flush();
        if (res_x >= (ny * ny)) {
            // std::cout << " SKIP" << std::endl;
            continue;
        }
        // std::cout << std::endl;
        for (int brr = 0; brr < block_size; brr++) {
          int res_y = by * block_size + brr;
          int res_i = res_x + res_y;
          // std::cout << " ry: " << res_y << " ri: " << res_i;
          // std::cout.flush();
          if (res_y >= ny) {
            // std::cout << " SKIP" << std::endl;
            continue;
          }

          double res = 0;
          for (int vp = 0; vp < vec_size; vp++) {
            res += ress[br][brr][vp];
            // std::cout << ress[br][brr][vp];
          }
          // std::cout << " - res: " << res << std::endl;
          result[res_i] = res;
        }
      }
    }
  }
  end = std::chrono::steady_clock::now();
  std::cerr << (end - start).count() << std::endl;
}
