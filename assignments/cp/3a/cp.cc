#include <algorithm>
#include <cmath>
#include <immintrin.h>
#include <iostream>
#include <limits>
#include <vector>

#include <x86intrin.h>

#include "vector.h"

constexpr int vec_size = 4;
constexpr double infty = std::numeric_limits<double>::infinity();
constexpr double lowest = std::numeric_limits<double>::lowest();

static inline double4_t swap2(double4_t x) {
  return _mm256_permute2f128_pd(x, x, 0b00000001);
}

static inline double4_t swap1(double4_t x) {
  return _mm256_permute_pd(x, 0b0101);
}

static void normalize_matrix(int ny, int nx, const float *data, int nny,
                             std::vector<double4_t> *output) {
  double sums[ny], means[ny], mins[ny], maxs[ny];

#pragma omp parallel for schedule(static, 1)
  for (int y = 0; y < ny; y++) {
    sums[y] = 0;
    means[y] = 0;
    mins[y] = infty;
    maxs[y] = lowest;

    int norm_y = y / vec_size;
    int vec_y = y % vec_size;

    for (int x = 0; x < nx; x++) {
      double xx = data[nx * y + x];

      mins[y] = std::min(mins[y], xx);
      maxs[y] = std::max(maxs[y], xx);
      means[y] += xx;
    }
    means[y] /= nx;

    // Arithmetic mean == 0
    for (int x = 0; x < nx; x++) {
      double xx = data[nx * y + x];

      double val = (xx - means[y]) / (maxs[y] - mins[y]);

      (*output)[nny * norm_y + x][vec_y] = val;
      sums[y] += std::pow(val, 2);
    }
    sums[y] = std::sqrt(sums[y]);

    // Sum of squares == 1
    for (int x = 0; x < nx; x++) {
      (*output)[nny * norm_y + x][vec_y] /= sums[y];
    }
  }
}

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in data[x + y*nx]
- correlation between rows i and row j has to be stored in result[i + j*ny]
- only parts with 0 <= j <= i < ny need to be filled
*/
void correlate(int ny, int nx, const float *data, float *result) {
  int row_vec_n = (ny + vec_size - 1) / vec_size;

  int normdata_size = row_vec_n * nx; // num of vectors in the whole matrix

  std::cout << "R: " << row_vec_n << " C: " << nx << " S: " << normdata_size
            << std::endl;
  std::vector<double4_t> normdata(normdata_size, double4_0);

  normalize_matrix(ny, nx, data, row_vec_n, &normdata);

  asm("# start working");
  //#pragma omp parallel for collapse(2) schedule(static, 1)
  for (int ay = 0; ay < row_vec_n; ay++) {
    for (int by = 0; by < row_vec_n; by++) {
      if (by < ay) {
        continue;
      }

      int A_vec_pos = nx * (ay / vec_size);
      int B_vec_pos = nx * (by / vec_size);

      double4_t ress[vec_size];
      asm("# prepare accumulators");
      for (int br = 0; br < vec_size; br++) {
        ress[br] = double4_0;
      }

      // TODO(V2): Better use caches
      asm("# columns loop");
      for (int x = 0; x < nx; x++) {
        asm("# read data");
        double4_t A = normdata[A_vec_pos + x];
        double4_t B[vec_size];

        B[0] = normdata[B_vec_pos + x];
        B[1] = swap1(B[0]);
        B[2] = swap2(B[0]);
        B[3] = swap2(swap1(B[0]));

        for (int br = 0; br < vec_size; br++) {
          for (int vr = 0; vr < vec_size; vr++) {
            std::cout << B[br][vr] << " ";
          }
          std::cout << std::endl;
        }
        std::cout << std::endl;
        /*for (int br = 0; br < block_size; br++) {
          for (int bc = 0; bc < block_size; bc++) {
            int pos_A = A_block_i_base + br * col_vec_n + x * block_size + bc;
            int pos_B = B_block_i_base + br * col_vec_n + x * block_size + bc;

            A[br][bc] = normdata[pos_A];
            B[br][bc] = normdata[pos_B];
          }
        }*/

        asm("# count");
        for (int br = 0; br < vec_size; br++) {
          ress[br] += A * B[br];
        }

        /*for (int ar = 0; ar < block_size; ar++) {
          int A_pos_ref = A_block_i_base + ar * col_vec_n;
          for (int br = 0; br < block_size; br++) {
            int B_pos_ref = B_block_i_base + br * col_vec_n;
            if (B_pos_ref < A_pos_ref) {
              continue;
            }

            for (int bc = 0; bc < block_size; bc++) {
              ress[ar][br] += A[ar][bc] * B[br][bc];
            }
          }
        }*/
      }

      asm("# save results");
      int res_pos_y = ay * nx * vec_size;
      int res_pos_x = by * vec_size;
      // Move to the right
      for (int br = 0; br < vec_size; br++) {
        // Move one step down and to the right (only by the diagonal)
        std::cout << "ROUND " << br << std::endl;
        for (int vr = 0; vr < vec_size; vr++) {
          int res_i = res_pos_y + res_pos_x + vr * ny + vr;

          if (ay == by) {
            res_i += br;
          }
          std::cout << "RI(" << res_i << "): " << ress[br][vr] << " - "
                    << res_pos_y << " " << res_pos_x << " " << br << " " << vr
                    << std::endl;

          // Check if we're overflowing the side of the result matrix
          if (res_pos_x + br + vr >= ny || br + vr >= vec_size) {
            std::cout << "YOINK" << std::endl;
            break;
          }
          result[res_i] = ress[br][vr];
        }
      }

      /*for (int br = 0; br < block_size; br++) {
        int res_x = ay * ny * block_size + br * ny;

        // std::cout << "rx: " << res_x;
        // std::cout.flush();
        if (res_x >= (ny * ny)) {
          // std::cout << " SKIP" << std::endl;
          continue;
        }
        // std::cout << std::endl;
        for (int brr = 0; brr < block_size; brr++) {
          int res_y = by * block_size + brr;
          int res_i = res_x + res_y;
          // std::cout << " ry: " << res_y << " ri: " << res_i;
          // std::cout.flush();
          if (res_y >= ny) {
            // std::cout << " SKIP" << std::endl;
            continue;
          }

          double res = 0;
          for (int vp = 0; vp < vec_size; vp++) {
            res += ress[br][brr][vp];
          }
          // std::cout << " - res: " << res << std::endl;
          result[res_i] = res;
        }
      }*/
    }
  }
}
