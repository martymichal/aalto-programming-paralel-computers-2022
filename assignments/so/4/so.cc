#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <vector>

#include <omp.h>

typedef unsigned long long data_t;

void psort(int n, data_t *data) {
  if (n <= 1) {
    return;
  }

  int thread_n = omp_get_max_threads();
  int width = (n + thread_n - 1) / thread_n;
#pragma omp parallel for
  for (int i = 0; i < n; i += width) {
    int end = std::min(i + width, n);

    std::sort(data + i, data + end);
  }

  int run_n = 0;
  for (int group_n = (n + width - 1) / width; group_n > 1;
       group_n = (group_n + 1) / 2) {
    run_n++;
  }

  if (run_n % 2 == 1) {
#pragma omp parallel for
    for (int i = 0; i < n; i += width * 2) {
      int mid = std::min(i + width, n);
      int end = std::min(i + 2 * width, n);

      std::inplace_merge(data + i, data + mid, data + end);
    }
    width *= 2;
    run_n--;
  }

  data_t *tmp = (data_t *)malloc(n * sizeof(data_t));
  data_t **a = &data;
  data_t **b = &tmp;
  data_t **c = NULL;
  for (; width < n; width *= 2) {
#pragma omp parallel for
    for (int i = 0; i < n; i += width * 2) {
      int mid = std::min(i + width, n);
      int end = std::min(i + 2 * width, n);
      std::merge((*a) + i, (*a) + mid, (*a) + mid, (*a) + end, (*b) + i);
    }
    c = b;
    b = a;
    a = c;
    run_n--;
  }
  free(tmp);
}
