#include <algorithm>
#include <vector>

/*
This is the function you need to implement. Quick reference:
- input rows: 0 <= y < ny
- input columns: 0 <= x < nx
- element at row y and column x is stored in in[x + y*nx]
- for each pixel (x, y), store the median of the pixels (a, b) which satisfy
  max(x-hx, 0) <= a < min(x+hx+1, nx), max(y-hy, 0) <= b < min(y+hy+1, ny)
  in out[x + y*nx].
*/
void mf(int ny, int nx, int hy, int hx, const float *in, float *out) {
  asm("# loop rows - begin");
  #pragma omp parallel for
  for (int y = 0; y < ny; y++) {
    int top = std::max(y - hy, 0);
    int bottom = std::min(y + hy, ny - 1);
    asm("# loop columns - begin");
    for (int x = 0; x < nx; x++) {
      int left = std::max(x - hx, 0);
      int right = std::min(x + hx, nx - 1);

      std::vector<double> window((2 * hx + 1) * (2 * hy + 1), -1);

      int i = 0;
      asm("# prep window row - begin");
      for (int wy = top; wy <= bottom; wy++) {
        asm("# prep window col - begin");
        for (int wx = left; wx <= right; wx++) {
          window[i] = in[wy * nx + wx];
          i++;
        }
        asm("# prep window col - end");
      }
      asm("# prep window row - end");

      double median = 0;
      auto mid_it = window.begin() + i / 2;
      asm("# nth element");
      std::nth_element(window.begin(), mid_it, window.begin() + i);
      median = *mid_it;

      if (i % 2 == 0) {
	asm("# even number of elements");
        auto mid_left_it = window.begin() + (i - 1) / 2;
        std::nth_element(window.begin(), mid_left_it, window.begin() + i);
        median = (*mid_left_it + median) / 2;
      }

      asm("# answer");
      out[y * nx + x] = median;
    }
  }
}
