#include <iostream>

struct Result {
  float avg[3];

};

/*
This is the function you need to implement. Quick reference:
- x coordinates: 0 <= x < nx
- y coordinates: 0 <= y < ny
- horizontal position: 0 <= x0 < x1 <= nx
- vertical position: 0 <= y0 < y1 <= ny
- color components: 0 <= c < 3
- input: data[c + 3 * x + 3 * nx * y]
- output: avg[c]
*/
Result calculate(int ny, int nx, const float *data, int y0, int x0, int y1,
                 int x1) {
  double r = 0, g = 0, b = 0;

  // Space between the last pixel on a row and the first pixel on the next one
  int row_diff = (nx - x1) * 3 + x0 * 3;
  // Initial position
  int pos = x0 * 3 + 3 * nx * y0;
  // std::cout << "D: " << row_diff << std::endl;
  for (int y = y0; y < y1; y++) {
    for (int x = x0; x < x1; x++) {
      r += data[pos];
      g += data[pos + 1];
      b += data[pos + 2];

      // std::cout << pos << " - " << r << " " << g << " " << b << std::endl;

      // Move forward on a row
      pos += 3;
    }
    // Go to the next row
    pos += row_diff;
  }

  int pix_n_num = (x1 - x0) * (y1 - y0);
  r = r / pix_n_num;
  g = g / pix_n_num;
  b = b / pix_n_num;

  // std::cout << pix_n_num << " - " << r << " " << g << " " << b << std::endl;

  return Result {{float(r), float(g), float(b)}};
}
